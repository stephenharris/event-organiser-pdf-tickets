<?php
/**
 * Adds a 'Download Tickets' button to the booking admin screen
 */
function eventorganiser_booking_actions_misc(){
	//TODO Add nonces
	echo '<div class="misc-pub-section">';
	printf(
		'<a href="%s" class="button secondary">%s</a>',
		add_query_arg( 'download-tickets', '1'),
		__( 'Download tickets', 'eventorganiserp' )
	);
	echo '</div>';
}
add_action( 'eventorganiser_booking_actions_misc', 'eventorganiser_booking_actions_misc' );


