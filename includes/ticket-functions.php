<?php

/**
 * Generates the PDF tickets for a given booking ID
 * @param int $booking_id
 * @return HTML2PDF PDF of tickets for specified booking.
 */
function eo_pro_generate_booking_pdf( $booking_id ){

	if( !class_exists( 'HTML2PDF' ) )
		require_once( EVENT_ORGANISER_PDF_TICKETS_DIR . 'includes/libraries/html2pdf/html2pdf.class.php');
	
	$html2pdf = new HTML2PDF('P', 'A4' );

	$html2pdf->pdf->SetDisplayMode('fullpage');
	$file = eo_locate_template( 'eo-ticket.php' );
	
	ob_start();
	include( $file );
	$content = ob_get_contents();
	ob_end_clean();

	$html2pdf->WriteHTML($content);

	return $html2pdf;
}

/**
 * Generates QR code value. For use in `<qrcode>` tag
 * 
 * <code>
 * <qrcode value="<?php echo esc_attr( eventorganiser_ticket_qr_code( $ticket ) ); ?>" ec="L" style="border: none; width: 50mm;"></qrcode>
 * </code>
 * 
 * @param object $ticket Ticket object
 * @return string The QR code value
 */

function eventorganiser_ticket_qr_code( $ticket ){
	$ticket_arr = array(
			$ticket->booking_ticket_id,
			$ticket->booking_id,
			$ticket->event_id,
			$ticket->occurrence_id,
	);
	$qrvalue = json_encode( $ticket );
	$qrvalue = implode( '|', $ticket_arr );
	return $qrvalue;
}
