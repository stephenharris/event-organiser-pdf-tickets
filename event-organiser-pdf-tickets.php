<?php 
/*
Plugin Name: Event Organiser PDF tickets
Plugin URI: http://www.wp-event-organiser.com
Version: 1.0
Description: Produces PDF Tickets
Author: Stephen Harris
Author URI: http://www.stephenharris.info
*/
/*  Copyright 2013 Stephen Harris (contact@stephenharris.info)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

function _eventorganiser_pdf_tickets_set_constants(){
	/*
	 * Defines the plug-in directory url
	* <code>url:http://mysite.com/wp-content/plugins/event-organiser-pdf-tickets</code>
	*/
	define( 'EVENT_ORGANISER_PDF_TICKETS_URL', plugin_dir_url( __FILE__ ) );
}
add_action( 'after_setup_theme', '_eventorganiser_pdf_tickets_set_constants' );
/*
 * Defines the plug-in directory path
* <code>/home/mysite/public_html/wp-content/plugins/event-organiser-pdf-tickets</code>
*/
define( 'EVENT_ORGANISER_PDF_TICKETS_DIR', plugin_dir_path( __FILE__ ) );


// PDF ticket functions
require_once( EVENT_ORGANISER_PDF_TICKETS_DIR . 'includes/ticket-functions.php');

/**
 * Register /templates subfolder in Event Organiser template stack.
 * Looks here for eo-ticket.php
 * Hooked onto `eventorganiser_template_stack`
 */
function eventorganiser_pdf_tickets_register_stack( $stacks ){
	$stacks[] = EVENT_ORGANISER_PDF_TICKETS_DIR . 'templates';
	return $stacks;
}
add_filter( 'eventorganiser_template_stack', 'eventorganiser_pdf_tickets_register_stack' );


// API & functions

//Add buttons to booking admin page
//require_once( EVENT_ORGANISER_PDF_TICKETS_DIR . 'admin/bookings.php');

//Actions
require_once( EVENT_ORGANISER_PDF_TICKETS_DIR . 'includes/actions.php');
?>
