<?php

/**
 * Add attachment to confirmed booking emails
 * Hooked onto eventorganiser_booking_confirmed_email_attachments.
 */
function eventorganiser_api_attach_pdf_tickets( $attachments, $booking_id ){

	//We need to add the string attachment directly $phpmailer
	global $event_organiser_pro_email_booking_id;
	$event_organiser_pro_email_booking_id = $booking_id;
	add_action( 'phpmailer_init', 'eventorganiser_api_attach_pdf_file', 10 );
	add_action( 'phpmailer_init', 'eventorganiser_remove_api_attach_pdf_file', 11 );
	return $attachments;
}
add_filter( 'eventorganiser_booking_confirmed_email_attachments', 'eventorganiser_api_attach_pdf_tickets', 10, 2 );


/**
 * Helper function, retrieves PDF and attaches it to email
 * Hooked onto phpmailer_init.
 */
function eventorganiser_api_attach_pdf_file( $phpmailer ){

	global $event_organiser_pro_email_booking_id;
	if( isset( $event_organiser_pro_email_booking_id ) && $event_organiser_pro_email_booking_id ){
		$pdf = eo_pro_generate_booking_pdf( $event_organiser_pro_email_booking_id );
		$filename = get_option( 'blogname' ).'-'.$event_organiser_pro_email_booking_id.'.pdf';
		$filename = apply_filters( 'eventorganiser_pdf_ticket_filename', $filename, $event_organiser_pro_email_booking_id );
		$filename = sanitize_file_name( $filename );
		$phpmailer->AddStringAttachment( $pdf->Output( '', 'S' ), $filename );
		$event_organiser_pro_email_booking_id = false;
	}
}

/**
 * Removes the above helper function, so it isn't called if phpmail_init is triggered again.
 * Hooked onto phpmailer_init.
 */
function eventorganiser_remove_api_attach_pdf_file(){
	remove_action( 'phpmailer_init', 'eventorganiser_api_attach_pdf_file', 10 );
}

/**
 * "Download tickets" listener.
 */
function eventorganiser_pro_maybe_tickets_download(){
	
	//TODO Check nonces
	if( !empty( $_GET['booking_id'] ) && !empty( $_GET['download-tickets'] ) ){
		if( current_user_can( 'manage_options') ){
			$booking_id = (int) $_GET['booking_id'];

			$pdf = eo_pro_generate_booking_pdf( $booking_id );
			$filename = get_option( 'blogname' ).'-'.$booking_id.'.pdf';
			$filename = apply_filters( 'eventorganiser_pdf_ticket_filename', $filename, $booking_id );
			$filename = sanitize_file_name( $filename );
			$pdf->Output( $filename, 'D' );
			exit();
		}
	}
}
add_action( 'load-event_page_bookings', 'eventorganiser_pro_maybe_tickets_download' );

