<?php

$format = get_option('date_format').' '.get_option('time_format');

$booking_reference = "#".$booking_id;
$booked = get_the_time( $format, $booking_id );

$bookee = eo_get_booking_meta( $booking_id, 'bookee' );
$bookee = eo_get_booking_meta( $booking_id, 'bookee_display_name' );

$event_id = eo_get_booking_meta( $booking_id, 'event_id' );
$occurrence_id = eo_get_booking_meta( $booking_id, 'occurrence_id' );
$event = get_the_title( $event_id );
$start = eo_reoccurs( $event_id ) ? eo_get_schedule_start( $format, $event_id ) : eo_get_the_start( $format, $event_id, null, $occurrence_id );

$venue = eo_get_venue_name( eo_get_venue( $event_id ) );
$venue_address = eo_get_venue_address( eo_get_venue( $event_id ) );
$venue_address = implode( ', ', array_filter( $venue_address ) );

$tickets = eo_get_booking_tickets( $booking_id, false );
?>
<style type="text/css">

body {margin-top: 0px;margin-left: 0px; }

h1 { padding: 0; margin: 0; color: #DD0000; font-size: 7mm; }
h2 { padding: 0; margin: 0; color: #222222; font-size: 5mm; position: relative; display; inline-block; }


.ticket{border-radius: 6mm;position:relative;margin: 110px auto; font-family: Arial, Helvetica, Sans-Serif;font-size: 5mm; border: 1mm solid #333;width: 190mm}
#logo{ text-align: right; margin-right:6mm; }

.footer-text{ color: #333; text-align:center; font-size:4mm;margin-bottom:2mm; }
.ticket-number{ font-weight: bold; color: #333; text-align:center }
.address{ font-style:italic; display:inline-block;font-size:85%;}
.ticket-details tr{ height: 10mm;}
.ticket-details th{ font-size: 7mm; }
.ticket-details td{ color: #333 }
div.zone { border: none;  background: #FFFFFF; border-collapse: collapse;  font-size: 2.7mm; margin-right: 6mm;}
div.ticket-details{
margin-left:6mm;
}

</style>
<?php foreach( $tickets as $ticket ): ?>
<page  orientation="P" backcolor="" style="font: arial;">
	
	<div class="ticket">

    <table style="width:120mm" cellspacing="" cellpadding="0">
        <tr class="brand">
            <td colspan="2" style="width: 100%">
			<div id="logo">
                    <img src="<?php echo EVENT_ORGANISER_PDF_TICKETS_URL . "assets/event-organiser.png";?>">
			</div>
            </td>
        </tr>
        <tr>
            <td style="width: 75%">
			<div class="ticket-details">
		    <table style="width: 99%; text-align:left; line-height:20px;" cellspacing="4mm" cellpadding="0">
			<tr>
				<th> Event </th>
				<td> <?php echo esc_html( $event ); ?> </td>
			</tr>
			<tr>
				<th> Date & Time</th>
				<td> <?php echo esc_html( $start ); ?> </td>
			</tr>
			<?php if( eo_get_venue( $event_id ) ){ ?>
				<tr>
					<th style="width:25%"> Venue </th>
					<td style="width:70%"> 
						<?php echo esc_html( $venue ); ?>
						<br><span class="address"><?php echo esc_html( $venue_address ); ?></span>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<th> Ticket </th>
				<td> <?php echo $ticket->ticket_name; ?> </td>
			</tr>
			</table>
		</DIV>
            </td>
            <td style="width: 60mm">
                <div class="zone" style="vertical-align: middle;text-align: center;">
		    		<qrcode value="<?php echo esc_attr( eventorganiser_ticket_qr_code( $ticket ) ); ?>" ec="L" style="border: none; width: 50mm;"></qrcode>
                </div>
			<p class="ticket-number"><?php echo $ticket->ticket_reference; ?></p>
            </td>
        </tr>
    </table>

	<div class="footer-text"><?php echo "Booked by $bookee on $booked. Booking reference {$booking_reference}.";?></div>
	</div>
</page>
<?php endforeach; ?>
